﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.IO;

namespace DemoGit.Console.Data
{
    public class Car
    {
        [Required]
        [MaxLength(20)]
        public string Brand { get; set; }

        [Required]
        public Color Color { get; set; }

        public string Model { get; set; }

        public DateTime Year { get; set; }

        public int Volume { get; set; }
    }
}
