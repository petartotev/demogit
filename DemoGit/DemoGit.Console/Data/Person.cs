﻿using System.ComponentModel.DataAnnotations;

namespace DemoGit.Console.Data
{
    public class Person
    {
        [Required]
        [MinLength(3)]
        [MaxLength(50)]
        public string Name { get; set; }
    }
}
